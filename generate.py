#!/usr/bin/env python3

import shutil, os
import marko, fontforge

from pathlib import Path
from string import Template

global_name = "Clbuttic"
git_repo = "https://gitlab.com/warningnonpotablewater/clbuttic-fonts"

build_folder = "build"
css_filename = f"{build_folder}/fonts.css"

css_template = Path("src/template.css").read_text()

def main():
    fetch_fonts()
    remove_old_build()
    build_license()

    tex_gyre = "src/tex-gyre/fonts/opentype/public/tex-gyre/texgyre"

    tex_gyre_heros = f"{tex_gyre}heros-"
    tex_gyre_termes = f"{tex_gyre}termes-"
    courier10pbt = "src/bitstream-type1/c0"

    Font("Sans",
        regular=f"{tex_gyre_heros}regular.otf",
        italic=f"{tex_gyre_heros}italic.otf",
        bold=f"{tex_gyre_heros}bold.otf",
        bold_italic=f"{tex_gyre_heros}bolditalic.otf",

        fallback=[
            "Helvetica",
            "Helvetica Neue",
            "Nimbus Sans",
            "Arial",
            "sans-serif"
        ]
    ).v_adjust(30).save()

    Font("Serif",
        regular=f"{tex_gyre_termes}regular.otf",
        italic=f"{tex_gyre_termes}italic.otf",
        bold=f"{tex_gyre_termes}bold.otf",
        bold_italic=f"{tex_gyre_termes}bolditalic.otf",

        fallback=[
            "Times New Roman",
            "Nimbus Roman",
            "serif"
        ]
    ).v_adjust(100).save()

    Font("Mono",
        regular=f"{courier10pbt}419bt_.pfb",
        italic=f"{courier10pbt}582bt_.pfb",
        bold=f"{courier10pbt}583bt_.pfb",
        bold_italic=f"{courier10pbt}611bt_.pfb",

        fallback=[
            "Courier New",
            "Courier",
            "Nimbus Mono",
            "monospace"
        ]
    ).save()

def fetch_fonts():
    os.system("git submodule update --recursive --init")

def remove_old_build():
    try:
        shutil.rmtree(build_folder)
    except FileNotFoundError:
        pass

    os.makedirs(build_folder, exist_ok=True)

def build_license():
    raw_license = Path("src/license.md").read_text()

    seasoned_license = Template(raw_license).substitute({
        "font_name": global_name,
        "git_repo": git_repo
    })
    cooked_license = marko.convert(seasoned_license)

    Path(f"{build_folder}/index.html").write_text(cooked_license)

class Font:
    name = ""

    variants = []

    regular = None
    italic = None
    bold = None
    bold_italic = None

    fallback = []

    def __init__(self, name, regular, italic, bold, bold_italic, fallback):
        self.name = name

        self.regular = fontforge.open(regular)
        self.italic = fontforge.open(italic)
        self.bold = fontforge.open(bold)
        self.bold_italic = fontforge.open(bold_italic)

        self.variants = [
            self.regular,
            self.italic,
            self.bold,
            self.bold_italic
        ]

        self.fallback = fallback

        self._rename()
        self._filter()

    def _rename(self):
        self._rename_variant(self.regular, "Regular")
        self._rename_variant(self.italic, "Italic")
        self._rename_variant(self.bold, "Bold")
        self._rename_variant(self.bold_italic, "BoldItalic")

    def _rename_variant(self, variant, variant_name):
        variant.familyname = f"{global_name}{self.name}"
        variant.fontname = f"{variant.familyname}-{variant_name}"
        variant.fullname = f"{global_name} {self.name} {variant_name}"

        variant.appendSFNTName("English (US)", "UniqueID", None)
        variant.appendSFNTName("English (US)", "Preferred Family", None)

    def _filter(self):
        for variant in self.variants:
            variant.encoding = "UnicodeBMP"

            if variant.cidsubfontcnt > 0:
                variant.cidFlatten()

            variant.selection.select(("unicode", "ranges"),
                0x20, 0x7e,
                0xa0, 0xff
            )
            variant.selection.select(("more", "unicode"),
                0x0178, 0x0192, 0x02c6,
                0x02dc, 0x201a, 0x2026,
                0x2030, 0x2039, 0x203a,
                0x20ac, 0x2122, 0x0152,
                0x0153, 0x0160, 0x0161,
                0x017d, 0x017e, 0x2013,
                0x2014, 0x2018, 0x2019,
                0x201c, 0x201d, 0x201e,
                0x2020, 0x2021, 0x2022
            )

            variant.selection.invert()
            variant.clear()
            variant.selection.none()

    def v_adjust(self, amount):
        for variant in self.variants:
            variant.hhea_ascent -= amount
            variant.hhea_descent -= amount

            variant.os2_winascent -= amount
            variant.os2_windescent += amount

        return self

    def save(self):
        def kebabify(string):
            return string.lower().replace(" ", "-")

        family = f"{global_name} {self.name}"
        variant = f"{global_name}{self.name}"

        variable = kebabify(family)
        fallback = ", ".join([family] + self.fallback)

        prefix = f"{build_folder}/{variant}-"

        self.regular.generate(f"{prefix}Regular.woff2")
        self.italic.generate(f"{prefix}Italic.woff2")
        self.bold.generate(f"{prefix}Bold.woff2")
        self.bold_italic.generate(f"{prefix}BoldItalic.woff2")

        css = Template(css_template).substitute({
            "family": family,
            "variant": variant,
            "variable": variable,
            "fallback": fallback
        })

        with open(css_filename, "a") as css_output:
            css_output.write(css)

if __name__ == "__main__":
    main()
