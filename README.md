# This project is no longer developed

I have since found other alternatives to proprietary fonts with much
better internationalization support:

* Helvetica - [Inter Display]
* Times New Roman - [STIX 1]
* Courier New - Мастерская Дмитрия Новикова's [Courier Prime fork]

[Inter Display]: https://rsms.me/inter/
[STIX 1]: https://github.com/stipub/stixfonts/tree/f120869ef6853e187514917dc8c2b99adfded140/archive/STIXv1.1.1/Fonts/STIX-Word
[Courier Prime fork]: http://dimkanovikov.pro/courierprime/

---

Clbuttic Fonts is a distribution of classic-looking fonts made by
other people, that were adjusted to work better on simple websites.

## Dependencies

* Python 3
* FontForge
* [Marko]

[Marko]: https://pypi.org/project/marko/

## How to build

```
./generate.py
```

## How to use

1. Build the fonts.
2. Grab the `build` folder and put it in your website's fonts folder
   under an appropriate name.
3. Point your licensing information page to the `index.html` file
   inside the `build` folder in the section about the fonts.
4. Import the font stylesheet called `fonts.css` from the build
   folder.
5. Use a CSS variable when you need to apply a font. For example, if
   you need Clbuttic Sans, use `font-family: var(--clbuttic-sans);`.

## Original fonts

* Clbuttic Sans - TeX Gyre Heros (with adjusted vertical metrics)
* Clbuttic Serif - TeX Gyre Termes (with adjusted vertical metrics)
* Clbuttic Mono - Courier 10 Pitch BT (converted to OpenType)

Additionally, the fonts were filtered to only include Windows-1252
characters and compressed to the WOFF2 format.

## Meaning behind the name

See [Scunthorpe problem](https://en.wikipedia.org/wiki/Scunthorpe_problem).

## License

Note: the fonts themselves are covered under a [separate license].

[separate license]: src/license.md

[Zero-Clause BSD](LICENSE).
